extern crate stderrlog;

use mqtt_receiver::SimpleMQTTSource;
use serde::{Deserialize, Serialize};
use log::{error, info};
use serde_json::Error;

#[derive(Serialize, Deserialize, Debug)]
struct EnvMsg {
    temp: f64,
    pres: f64,
    gas_res: f64,
    hum: f64,
    tst: i64,
    water: i64,
}

fn main() {
    stderrlog::new().verbosity(1).quiet(false).init().unwrap();

    let src = SimpleMQTTSource::new();
    let connection = sqlite::open("test.sqlite").unwrap();
    connection.execute(
        "
        CREATE TABLE IF NOT EXISTS record (
            record_timestamp TIMESTAMP,
            temp REAL,
            pressure REAL,
            gas_resistance REAL,
            humidity REAL,
            water_reading REAL
        )"
    ).unwrap();
    connection.execute(
        "
        CREATE VIEW IF NOT EXISTS recent_record as
            SELECT * FROM record WHERE record_timestamp > datetime('now', '-1 day');"
    ).unwrap();

    for msg_str in src.msg_src.iter() {
        let msg: Result<EnvMsg, Error> = serde_json::from_str(&msg_str);
        match msg {
            Ok(m) => write_message_logged(&m, &connection),
            Err(e) => error!("Error: {:?}", e),
        }
    }
}

fn write_message_logged(m: &EnvMsg, connection: &sqlite::Connection) {
    match write_message(m, connection) {
        Ok(_) => (), Err(e) => error!("Failed to write: {:?}", e),
    };
}

fn write_message(m: &EnvMsg, connection: &sqlite::Connection) -> Result<(), sqlite::Error> {
    info!("Got message {:?}", m);
    let mut statement = connection
        .prepare("INSERT INTO record VALUES (datetime(?, 'unixepoch'), ?, ?, ?, ?, ?)")?;
    statement.bind(1, m.tst)?;
    statement.bind(2, m.temp)?;1
    statement.bind(3, m.pres)?;
    statement.bind(4, m.gas_res)?;
    statement.bind(5, m.hum)?;
    statement.bind(6, m.water)?;
    statement.next()?;
    Ok(())
}

use std::io::Write;
use std::{net, thread};

use log::{error, info, trace};

use mqtt::packet::*;
use mqtt::TopicFilter;
use mqtt::{Decodable, Encodable, QualityOfService};

use crossbeam::channel::unbounded;
use std::thread::JoinHandle;
use std::mem::replace;

pub struct SimpleMQTTSource {
    pub msg_src: crossbeam::Receiver<String>,
    join_handler: Option<thread::JoinHandle<()>>,
}

impl Drop for SimpleMQTTSource {
    fn drop(&mut self) {
        let old_handler = replace(&mut self.join_handler, None);
        match old_handler {
            Some(h) => {h.join().unwrap_err();},
            None => ()
        };
    }
}

impl SimpleMQTTSource {
    pub fn new() -> SimpleMQTTSource {
        let server_addr = "pi.local:1883";
        let client_id = "HOME";
        let mut stream = net::TcpStream::connect(server_addr).unwrap();
        let conn = ConnectPacket::new("MQTT", client_id);
        let mut buf = Vec::new();

        conn.encode(&mut buf).unwrap();
        stream.write_all(&buf[..]).unwrap();

        let filters: Vec<(TopicFilter, QualityOfService)> = vec![(
            TopicFilter::new("riri/environment").unwrap(),
            QualityOfService::Level0,
        )];

        let sub = SubscribePacket::new(10, filters);
        let mut buf = Vec::new();
        sub.encode(&mut buf).unwrap();
        stream.write_all(&buf[..]).unwrap();

        loop {
            let packet = match VariablePacket::decode(&mut stream) {
                Ok(pk) => pk,
                Err(err) => {
                    error!("Error in receiving packet {:?}", err);
                    continue;
                }
            };
            trace!("PACKET {:?}", packet);

            if let VariablePacket::SubackPacket(ref ack) = packet {
                if ack.packet_identifier() != 10 {
                    panic!("SUBACK packet identifier not match");
                }

                info!("Subscribed!");
                break;
            }
        }

        let (sender, recvr) = unbounded();

        let threadlet: JoinHandle<()> = thread::spawn(move || loop {
            let packet = match VariablePacket::decode(&mut stream) {
                Ok(pk) => pk,
                Err(err) => {
                    error!("Error in receiving packet {}", err);
                    continue;
                }
            };
            trace!("PACKET {:?}", packet);

            match packet {
                VariablePacket::PingrespPacket(..) => {
                    info!("Receiving PINGRESP from broker ..");
                }
                VariablePacket::PublishPacket(ref publ) => {
                    let msg = match String::from_utf8(publ.payload_ref().to_owned()) {
                        Ok(msg) => msg,
                        Err(err) => {
                            error!("Failed to decode publish message {:?}", err);
                            continue;
                        }
                    };
                    info!("PUBLISH ({}): {}", publ.topic_name(), msg);
                    sender.send(msg).unwrap();
                }
                other => error!("{:?}", other),
            }
        });

        SimpleMQTTSource {
            msg_src: recvr,
            join_handler: Some(threadlet),
        }
    }
}

//
// Created by Richard Weiss on 2020-05-16.
//

#include <Adafruit_BME680.h>
#include <Adafruit_Sensor.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <Wire.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

#include "sensor.hpp"
#include "pubsub.hpp"

Adafruit_BME680 bme;

WiFiUDP ntpUDP;
NTPClient time_client(ntpUDP, "pool.ntp.org", 0, 3600);

bool sensor_setup() {
  // Set up oversampling and filter initialization
  bool setup = bme.begin();
  if (!setup)
    return false;

  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150); // 320*C for 150 ms

  time_client.begin();
  time_client.update();

  return true;
}

SensorData empty_data() {
  return SensorData{
      false, 0, 0, 0, 0, 0,
  };
}

SensorData sensor_read() {
  SensorData res = empty_data();
  if (!bme.performReading()) {
    res.readsuccess = false;
    return res;
  }

  res.readsuccess = true;
  res.timestamp = time_client.getEpochTime();
  res.temp = bme.temperature;
  res.pressure = bme.pressure / 100;
  res.humidity = bme.humidity;
  res.gas_resistance = bme.gas_resistance / 1000;
  res.water_reading = (int)analogRead(A0);

  return res;
}


size_t serialize(SensorData data, char* output, size_t buf_len) {
  static StaticJsonDocument<2048> doc;
  time_client.update();
  doc["temp"] = data.temp;
  doc["tst"] = data.timestamp;
  doc["pres"] = data.pressure;
  doc["gas_res"] = data.gas_resistance;
  doc["hum"] = data.humidity;
  doc["water"] = data.water_reading;

  return serializeJson(doc, output, buf_len);
}


SensorSender::SensorSender(int size) {
  this->buf_ptr = 0;
  this->size = size;
  this->buffer = new SensorData[size];
  while (!this->buffer) {
    Serial.println("Failed to allocate space for buffer");
  }
}

bool SensorSender::queue_data() {
  auto data = sensor_read();
  this->buffer[this->buf_ptr] = data;

  this->buf_ptr = (this->buf_ptr + 1) % this->size;
  return this->buf_ptr == this->size - 1;
}

void SensorSender::send_data(PubSubHandler handler) {
  static char buffer[1024];
  for (int i = 0; i < this->buf_ptr; i++) {
    auto data = this->buffer[i];
    serialize(data, buffer, 1024);
    bool res = handler.publish("riri/environment", buffer);
    if (res) {
      Serial.print("Publish!! ");
      Serial.println(buffer);
    } else {
      Serial.print("Publish failed! ");
      Serial.println(buffer);
    }
  }

  this->buf_ptr = 0;
}

SensorSender::~SensorSender() {
  delete[] this->buffer;
}

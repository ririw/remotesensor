#include <ESP8266WiFi.h>
#include <Arduino.h>
#include "sensor.hpp"
#include "wifi_pass.h"

WiFiClient wifi_client;
PubSubHandler ps_client;
SensorSender sender = SensorSender(60);

void setup_wifi();
void setup_sensor();
void callback(char *topic, byte *payload, unsigned int length);

void setup() {
  Serial.begin(9600);
  while (!Serial)
    ;

  pinMode(A0, INPUT);

  setup_wifi();
  setup_sensor();
  ps_client.setup(wifi_client, mqtt_server, 1883);
  Serial.println("Ready!");
}

void setup_wifi() {
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print("*");
  }

  Serial.println("");
  Serial.println("WiFi connection Successful");
  Serial.print("The IP Address of ESP8266 Module is: ");
  Serial.print(WiFi.localIP()); // Print the IP address
}

void setup_sensor() {
  Serial.println("BME680 test");
  if (!sensor_setup()) {
    Serial.println("Could not find a valid BME680 sensor, check wiring!");
    while (1)
      ;
  }
}

void loop() {
  bool should_send = sender.queue_data();
  if (should_send) {
    sender.send_data(ps_client);
  }
  
  delay(1000);
}

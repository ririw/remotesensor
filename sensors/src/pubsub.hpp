#include <PubSubClient.h>
#include <Client.h>


#ifndef SENSORS_PUBSUB_H
#define SENSORS_PUBSUB_H

class PubSubHandler {
    PubSubClient client;
public:
    PubSubHandler& setup(Client& _client, const char * domain, uint16_t port);
    bool publish(const char* topic, const char* content);
private:
    void reconnect();
};

#endif
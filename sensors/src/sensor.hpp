//
// Created by Richard Weiss on 2020-05-16.
//
#pragma once

#ifndef SENSORS_SENSOR_H
#define SENSORS_SENSOR_H

#include <Arduino.h>
#include "pubsub.hpp"

typedef struct {
  bool readsuccess;
  int timestamp;
  float temp;
  float pressure;
  float gas_resistance;
  float humidity;
  int water_reading;
} SensorData;

bool sensor_setup();
SensorData sensor_read();
size_t serialize(SensorData data, String &output);

class SensorSender {
  SensorData *buffer;
  int buf_ptr;
  int size;

  public:
    SensorSender(int size);
    ~SensorSender();
    bool queue_data();
    void send_data(PubSubHandler handler);
};

#endif

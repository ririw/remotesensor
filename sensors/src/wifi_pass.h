//
// Created by Richard Weiss on 2020-05-16.
//

#ifndef SENSORS_WIFI_PASS_H
#define SENSORS_WIFI_PASS_H

#include <stdint.h>
extern const char *password;
extern const char *ssid;
extern const char *mqtt_server;
#endif //SENSORS_WIFI_PASS_H

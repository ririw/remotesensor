#include <PubSubClient.h>
#include <Client.h>
#include "pubsub.hpp"

void callback(char *topic, byte *payload, unsigned int length);

PubSubHandler& PubSubHandler::setup(Client& _client, const char * domain, uint16_t port) {
    client.setClient(_client);
    client.setServer(domain, port);
    client.setCallback(callback);
    return *this;
}

bool PubSubHandler::publish(const char* topic, const char* content) {
    if (!client.connected()) this->reconnect();
    client.loop();
    return client.publish(topic, content);
}

void PubSubHandler::reconnect() {
    while (!client.connected()) {
        Serial.print("Attempting MQTT connection...");
        // Create a random client ID
        String client_id = "ESP8266Client-";
        client_id += String(random(0xffff), HEX);
        // Attempt to connect
        if (client.connect(client_id.c_str())) {
            Serial.println("connected");
            // Once connected, publish an announcement...
            client.publish("riri/environment", "sub");
        } else {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}


void callback(char *topic, byte *payload, unsigned int length) {
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);

  Serial.print("Message:");
  for (unsigned int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }

  Serial.println();
  Serial.println("-----------------------");
}
